FROM jenkins
USER root
MAINTAINER Kirill Shalnov <borhammere@gmail.com>

# enviroment variables to android sdk
ENV ANDROID_SDK_VERSION=r24.4.1
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools

# need to install jenkins plugins
COPY plugins.txt /usr/share/jenkins/plugins.txt

# install packages, install plugins and install android sdk
RUN dpkg --add-architecture i386 && \
	echo "================ APT-GET BLOCK ================" && \
	apt-get update -qq && \
 	apt-get install -y --force-yes expect git mercurial wget libc6-i386 lib32stdc++6 lib32gcc1 lib32ncurses5 lib32z1 python curl && \
    apt-get clean && \
	echo "================ JENKINS PLUGINS BLOCK ================" && \
	/usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt && \
	echo "================ ANDROID SDK BLOCK ================" && \
    cd /opt && \
    echo "~~~~~~~~~~~~~ Start loading SDK ~~~~~~~~~~~~~" && \
	wget -q https://dl.google.com/android/android-sdk_${ANDROID_SDK_VERSION}-linux.tgz -O /opt/android-sdk.tgz && \
	echo "\t~~~~~~~~~~~~~ Finish loading SDK ~~~~~~~~~~~~~" && \
	echo "\t~~~~~~~~~~~~~ Start unziping SDK ~~~~~~~~~~~~~" && \
	tar -xvzf /opt/android-sdk.tgz && \
	rm -f /opt/android-sdk.tgz && \
	echo "\t~~~~~~~~~~~~~ Finish unziping SDK ~~~~~~~~~~~~~" && \
	echo "\t~~~~~~~~~~~~~ Start updating SDK ~~~~~~~~~~~~~" && \
	echo y | android update sdk --no-ui --all --filter platform-tools && \
	echo y | android update sdk --no-ui --all --filter extra-android-support && \
	echo "$ANDROID_HOME" && \
	mkdir "$ANDROID_HOME/licenses" || true && \
	echo -e "\n8933bad161af4178b1185d1a37fbf41ea5269c55" > "$ANDROID_HOME/licenses/android-sdk-license" && \
	echo -e "\n84831b9409646a918e30573bab4c9c91346d8abd" > "$ANDROID_HOME/licenses/android-sdk-preview-license" && \
	chown -R jenkins $ANDROID_HOME

# drop back to the regular jenkins user - good practice 
USER jenkins 